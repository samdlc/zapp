<?php
namespace ZApp;

class Response {

	private $status = 200;
	private $protocolVersion = "1.1";
	private $locked = false;
	private $sent = false;
	private $headers;
	private $body = null;


	public function __construct() {
		$this->headers = array();
		$this->body = "";
	}


	public function send() {

		$this->locked = true;

		if(!$this->sent) {
			$this->sendHeaders();
			$this->sendBody();
		}

		$this->sent = true;

		return $this;

	}


	public function sendHeaders() {
		if(headers_sent()) {
			return $this;
		}

		header($this->httpStatus());

		foreach($this->headers as $k => $v) {
			header($k.': '.$v, false);
		}

		return $this;
	}


	public function sendBody() {
		echo (string) $this->body;
		return $this;
	}


	public function json($obj, $append = false) {

		$this->header('Content-Type', 'application/json');
		if(($append && !(null === $this->body))) {
			$decoded = json_decode($this->body, true);
			foreach($obj as $k => $v) {
				$decoded[$k] = $v;
			}
			$this->body = json_encode($decoded);
		} else {
			$this->body = json_encode($obj);
		}

		return $this;
	}


	public function body($string, $append = false) {
		if($append && !(null === $this->body)) {
			$this->body .= $string;
		} else {
			$this->body = $string;
		}
		return $this;
	}


	public function append($string) {
		return $this;
	}


	public function redirect($url, $code = 302) {
		return $this;
	}


	public function status($code) {
		return $this;
	}


	public function sendFile($handle) {
		return $this;
	}


	public function download($handle) {
		return $this;
	}


	public function header($key, $value) {
		$this->headers[$key] = $value;
		return $this;
	}


	public function lock() {
		$this->locked = true;
	}


	public function unlock() {
		$this->locked = false;
	}


	private function httpStatus() {
		return sprintf('HTTP/%s %s', $this->protocolVersion, $this->status);
	}
}