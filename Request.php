<?php
namespace ZApp;

class Request {


	public $uri;
	public $params;


	public function __construct() {

	}


	public function uri() {
		return $this->uri;
	}


	public function addParams(array $params) {
		if(!(isset($this->params) && is_array($this->params))) {
			$this->params = array();
		}
		foreach($params as $k => $v) {
			if(isset($this->params[$k])) {
				if(is_array($this->params[$k])) {
					$this->params[$k][] = $v;
				} else {
					$this->params[$k] = array($this->params[$k], $v);
				}
			} else {
				$this->params[$k] = $v;
			}
		}
		return true;
	}
}