<?php
namespace ZApp;

class Utils {
	public static function matchRoute($routeString, $uri, $submatch = false) {
		if($routeString === $uri) {
			return true;
		}
		// explode both strings on '/'
		$routeParts = array_slice(array_filter(explode('/', $routeString)), 0);
		$uriParts = array_slice(array_filter(explode('/', $uri)), 0);


		$numRouteParts = count($routeParts);
		$numUriParts = count($uriParts);

		// Route string should be equal or less parts than uri
		if($numRouteParts < $numUriParts && !$submatch) {
			// Unless a wildcard has been specified
			if(!(count($routeParts) > 0 && stripos(strrev($routeString), "*") === 0) || $routeParts[0] === $uriParts[0]) {
				return false;
			}
		}

		// loop over exploded route string
		for($i = 0; $i < $numRouteParts; $i++) {
			$routePart = $routeParts[$i];

			// if strings match, continue
			if($routePart === "*") {
				continue;
			} else if(isset($uriParts[$i]) && (strcmp($routePart, ($uriPart = $uriParts[$i])) === 0)) {
				continue;
			} else {
				return false;
			}
			// otherwise return false
		}
		return $i;
	}
}