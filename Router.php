<?php
namespace ZApp;

class Router {

	private $routes;
	public $uri;

	public function get($path, callable ...$callbacks) {
		return true;
	}

	public function route($path, callable ...$callbacks) {

		if(!(isset($path) && is_string($path) && count($callbacks > 0))) {
			return false;
		}

		if(!(isset($this->routes) && is_array($this->routes))) {
			$this->routes = array();
		}

		// Has an identical route been registered?
		if(isset($this->routes[$path])) {
			// Has more than one route been registered for this path?
			if(is_array($this->routes[$path])) {
				// It's quicker to append using the [] = val if only single callback
				if(count($callbacks) > 1) {
					$this->routes[$path] = array_merge($this->routes[$path], $callbacks);
				} else {
					$this->routes[$path][] = $callbacks[0];
				}
			} else {
				$this->routes[$path] = array_merge(array($this->routes[$path]), $callbacks);
			}
		} else {
			$this->routes[$path] = $callbacks;
		}
		return true;
	}

	public function handle($request, $response) {
		// loop over routes
		// if matchRoute doesnt return false add returned params to request params
		// Call each callback function in sequence
		foreach($this->getRoutes() as $route => $cbs) {
			$params = Utils::matchRoute($route, $this->uri);
			if(!($params === false)) {
				foreach($cbs as $callback) {
					$callback($request, $response);
				}
			}
		}
		return $response;
	}


	public function getRoutes() {
		if(isset($this->routes)){
			return $this->routes;
		}
		return array();
	}
}