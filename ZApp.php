<?php
namespace ZApp;
use ZApp\Request;
use ZApp\Router;
use ZApp\Response;
use ZApp\Utils;

class ZApp {


	private $routers;


	public function dispatch() {

		// Create a Request object
			// Calculate parameters here, and feed in to $req
			// hostname, ip, basepath, params[], querystring, body, protocol, originalurl, method, xhr?

		// Create a bare Response object

		// Loop over all registered paths and create call stack of callables

		// Call each one in turn, passing $req and $res

		$request = $this->initRequest();
		$response = new Response();

		$this->handleRequest($request, $response);

		$response->send();
	}


	private function handleRequest(Request $request, Response $response) {
		// loop over routers
		// if matchRoute doesnt return false add returned params to request params
		// Call each callback function in sequence
		foreach($this->getRouters() as $route => $routers) {
			$match = Utils::matchRoute($route, $request->uri(), true);
			if(!($match === false)) {

				$routeUri = implode("/", array_slice(explode("/", $request->uri), $match+1));
				$routeUri = strlen($routeUri) > 0 ? $routeUri : "";

				$max = is_string($routers) ? 1 : count($routers);
				if($max === 0) {
					continue;
				}
				if($max === 1) {
					$routers->uri = $routeUri;
					$routers->handle($request, $response);
					continue;
				}
				for($i = 0; $i < $max; $i++) {
					$routers[$i]->uri = $routeUri;
					$routers[$i]->handle($request, $response);
				}
			}
		}

		return true;
	}


	public function attach($path, $router) {

		if(!(isset($path) && is_string($path))) {
			return false;
		}

		if(!(isset($this->routers) && is_array($this->routers))) {
			$this->routers = array();
		}

		// Has an identical route been registered?
		if(isset($this->routers[$path])) {
			// Has more than one route been registered for this path?
			if(is_array($this->routers[$path])) {
				// It's quicker to append using the [] = val if only single value
				$this->routers[$path][] = $router;
			} else {
				$this->routers[$path] = array($this->routers[$path], $router);
			}
		} else {
			$this->routers[$path] = $router;
		}
		return true;
	}


	public function getRouters() {
		if(isset($this->routers)){
			return $this->routers;
		}
		return array();
	}


	private function initRequest() {
		$request = new Request();
		$uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
		$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';
		$protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : '';
		$timestamp = isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : 0;
		$query = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
		$userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
		$serverName = isset($_SERVER['SERVER_NAME'])? $_SERVER['SERVER_NAME'] : '';
		$serverPort = isset($_SERVER['SERVER_PORT'])? $_SERVER['SERVER_PORT'] : '';
		$prefix = isset($_SERVER['HTTPS']) ? (empty($_SERVER['HTTPS']) ? "http://" : "https://") : "http://";
		$requestBodyTmp = @file_get_contents('php://input');
		$requestBody = isset($requestBodyTmp) ? (empty($requestBodyTmp) ? '' : $requestBodyTmp) : '';

		$fullUrl = $prefix . $serverName . ":" . $serverPort . $uri;

		$params = $this->parseQuery($query);

		if($idx = strpos($uri, "?")) {
			$uri = substr($uri, 0, $idx);
		}
		$request->uri = $uri;
		$request->addParams($params);

		return $request;
	}


	private function parseQuery($query) {
		$params = array();
		$queries = array();

		if(strlen($query) > 2) {
			$queries = explode("&", $query);
			$numQs = count($queries);
			for($i = 0; $i < $numQs; $i++) {
				$kv = explode("=", $queries[$i], 2);
				if((count($kv) === 2) && strlen($kv[1]) > 0){
				    if(isset($params[$kv[0]])) {
						if(is_array($params[$kv[0]]) ) {
							$params[$kv[0]][] = $kv[1];
						}
						else {
							$params[$kv[0]] = array($params[$kv[0]], $kv[1]);
						}
				    }
					else {
						$params[$kv[0]] = $kv[1];
					}
				}
			}
		}
		return $params;
	}

}